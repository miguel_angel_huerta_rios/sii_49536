// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
 #include "Mundoservidor.h"
 #include "glut.h"
 #include <string.h>
 #include <math.h>
 #include <sys/mman.h>
 #include <sys/types.h>
 #include <sys/stat.h>
 #include "stdio.h"
 #include "stdlib.h"
 #include <unistd.h>
 #include <fcntl.h>
 #include <pthread.h>

#define TAM_BUFF 55
#define TAM_CAD 200

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void* hilo_comandos(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
}
CMundo::CMundo()
{
	Init();
	fd=open("/tmp/mififo1",O_WRONLY);
}

CMundo::~CMundo()
{
	
	
	//close(fd1);
	close(fd);
	
	//close(fd_t);
	sd.Close();
	sc.Close();
}
void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	char buff[TAM_BUFF];
	char buffer_cad[TAM_CAD];

	esfera.Mueve(0.025f);
	esfera.radio-=0.0005;

	//Regulacion del tamano de la pelota
	if(esfera.radio<0.2f) esfera.radio=0.2f;
	if(esfera.radio-0.2f<0.001f && esfera.radio-0.2f>-0.001f) esfera.radio=0.5f;

	//salida del juego por puntos
	if(puntos1==8 || puntos2==8){
		sprintf(buff,"Q");
		write(fd,buff,sizeof(buff));
		exit(0);
	}
	
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		char cad2[200];
		sprintf(cad2,"El jugador 2 marca 1 punto, lleva un total de %d puntos\n",puntos2);
		write(fd,cad2,sizeof(cad2));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		
		puntos1++;
		char cad3[200];
		sprintf(cad3,"Jugador 1 marca 1 punto, lleva %d puntos", puntos1);
		//Escritura en la tuberia para el logger
		write(fd,cad3,sizeof(cad3));
	}	
	
	

	//Escritura en la tuberia del Cliente Servidor
	char cad4[200];
	sprintf(cad4,"%f %f %f %f %f %f %f %f %f %f %d %d",esfera.centro.x, esfera.centro.y, jugador1.x1, jugador1.x2, jugador1.y1, jugador1.y2, jugador2.x1, jugador2.x2, jugador2.y1, jugador2.y2, puntos1, puntos2);
	sc.Send(cad4,sizeof(cad4));
		

	jugador1.RaquetaPulsante(0.025f);
	jugador2.RaquetaPulsante(0.025f);
	
	

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
//	case 's':jugador1.velocidad.y=-4;break;
//	case 'w':jugador1.velocidad.y=4;break;
//	case 'l':jugador2.velocidad.y=-4;break;
//	case 'o':jugador2.velocidad.y=4;break;

	}
}

void CMundo::Init()
{
	
	sd.InitServer((char*)"127.0.0.1",4000);
	sc=sd.Accept();
	sc.Receive(nomClient,sizeof(nomClient));
	std::cout<<nomClient<<std::endl;
	pthread_create(&thid1, NULL, hilo_comandos, this);
	
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;
	jugador1.pulso=1.0f;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	jugador2.pulso=1.0f;
	
	//Apertura de la tuberia
	

	//fd1=open("/tmp/FIFO_Cliente-Servidor",O_WRONLY);

	//fd_t=open("/tmp/FIFO_TECLAS",O_RDONLY);

	//Creacion del hilo
	

}



void CMundo::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad[200];
            //read(fd_t, cad, sizeof(cad));
            sc.Receive(cad,sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-3;
            if(key=='w')jugador1.velocidad.y=3;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}
